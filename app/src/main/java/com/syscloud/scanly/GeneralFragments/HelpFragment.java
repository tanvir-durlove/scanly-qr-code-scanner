package com.syscloud.scanly.GeneralFragments;

import android.preference.PreferenceFragment;
import android.os.Bundle;

import com.syscloud.scanly.R;


public class HelpFragment extends PreferenceFragment {

    public HelpFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.help);
    }
}
